import 'package:flutter/material.dart';

class PrimaryButton extends StatelessWidget {
  final Function() onPressed;
  final String text;
  final EdgeInsets margin;
  final EdgeInsets padding;
  final TextStyle textStyle;
  final ButtonTextTheme textTheme;
  final Color background;
  final ShapeBorder shape;
  final Widget child;

  const PrimaryButton(
      {Key key,
      this.onPressed,
      this.text,
      this.margin,
      this.padding,
      this.textStyle,
      this.textTheme,
      this.background,
      this.shape,
      this.child})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
        padding: margin ?? EdgeInsets.all(24),
        child: RaisedButton(
          shape: shape ??
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(8)),
          elevation: 4,
          textTheme: textTheme ?? ButtonTextTheme.primary,
          color: background ?? Colors.blueAccent,
          onPressed: onPressed ?? () {},
          padding:
              padding ?? EdgeInsets.symmetric(vertical: 12, horizontal: 40),
          child: child ??
              Text(
                text ?? "",
                style: textStyle ??
                    TextStyle(fontSize: 16, fontWeight: FontWeight.w600),
              ),
        ));
  }
}
