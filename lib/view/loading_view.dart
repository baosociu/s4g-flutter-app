import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';

class LoadingView extends StatelessWidget {
  @override
  Widget build(Object context) {
    return Container(
      margin: EdgeInsets.symmetric(vertical: 32),
      color: Colors.transparent,
      child: SpinKitRing(color: Colors.blueAccent, size: 64),
    );
  }
}
