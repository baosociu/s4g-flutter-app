import 'package:flutter/material.dart';
import 'package:s4g/page/authen_device.dart';
import 'package:s4g/page/buyer_advice_page.dart';
import 'package:s4g/page/confirm_account_page.dart';
import 'package:s4g/page/final_info_page.dart';
import 'package:s4g/page/initial_page.dart';
import 'package:s4g/page/questions_device_page.dart';

import 'page/initial_failure_page.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'SAG Flutter',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      debugShowCheckedModeBanner: false,
      onGenerateRoute: _onGenerateRoute,
      initialRoute: "init",
    );
  }

  Route _onGenerateRoute(RouteSettings settings) {
    Widget _screen;
    switch (settings.name) {
      case "init":
        _screen = InitialPage();
        break;
      case "init_failure":
        _screen = InitialFailurePage(error: settings.arguments);
        break;
      case "confirm_account":
        _screen = ConfirmAccountPage(authenModel: settings.arguments);
        break;
      case "questions_device":
        _screen = QuestionsDevicePage();
        break;
      case "final_info":
        _screen = FinalInfoPage();
        break;
      case "authen_device":
        _screen = AuthenDevicePage();
        break;
      case "buyer_advice":
        _screen = BuyerAdvicePage();
        break;
    }
    return MaterialPageRoute(
        builder: (_) => GestureDetector(
              child: _screen ??
                  Scaffold(
                    appBar: AppBar(
                      title: Text("Page not exist"),
                    ),
                  ),
              onTap: () => FocusScope.of(_).unfocus(),
            ));
  }
}
