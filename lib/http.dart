import 'dart:io';
import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class Http {
  static Http _internal;
  Http._();

  factory Http() {
    if (_internal == null) {
      _internal = Http._();
    }
    return _internal;
  }

  String token;
  String baseAPI = "http://45.77.106.23:8879/api/v1";

  Map _baseHeader() {
    Map _header = {
      'content-type': 'application/json',
      'Accept': '*/*',
    };
    if (Http().token != null) {
      _header["Authorization"] = Http().token;
    }
    return _header;
  }

  void clearSession() {
    _internal = null;
  }

  Duration get _timeoutDuration => Duration(seconds: 12);

  Map<String, String> _getHeaders(Map<String, String> header) {
    Map<String, String> _headers = {};
    _baseHeader().forEach((k, v) {
      if (v != null) {
        _headers[k] = v;
      }
    });
    (header ?? {}).forEach((k, v) {
      if (v != null) {
        _headers[k] = v;
      }
    });
    return _headers;
  }

  Map<String, dynamic> _getBodies(Map body) {
    Map<String, dynamic> _bodies = {};
    (body ?? {}).forEach((k, v) {
      if (v != null) {
        _bodies[k] = v;
      }
    });
    return _bodies;
  }

  Map<String, dynamic> _getParam(Map<String, dynamic> param) {
    if (param == null) return {};
    param.removeWhere((key, value) => value == null);
    return param;
  }

  Response _catchError(dynamic error, String path) {
    Response response;
    if (error is DioError) {
      response = error.response;
    } else if (error is TypeError) {
      response = Response(statusMessage: error.toString());
    } else {
      response = Response(statusMessage: Error.safeToString(error));
    }
    return response;
  }

  Future<Response> clockRequest(
      {@required BaseOptions options,
      @required String path,
      @required Future<Response> Function(String path, Dio dio) action}) async {
    var response = await action(path,
            Dio(options)..interceptors.add(LogInterceptor(responseBody: true)))
        .timeout(_timeoutDuration,
            onTimeout: () => Response(
                statusCode: HttpStatus.requestTimeout,
                statusMessage: "timeout"))
        .catchError((error) => _catchError(error, path));
    return response;
  }

  Future<Response> get(String url,
      {Map<String, String> headers,
      Map<String, dynamic> params}) async {
    Map<String, String> _headers = _getHeaders(headers);
    Map<String, dynamic> _params = _getParam(params);

    return clockRequest(
        path: url,
        options: BaseOptions(
            baseUrl: baseAPI,
            queryParameters: _params,
            followRedirects: false,
            headers: _headers),
        action: (path, dio) async {
          return await dio.get(path);
        });
  }

  Future<Response> post(String url,
      {Map body, Map<String, String> headers}) async {
    Map<String, String> _headers = _getHeaders(headers);
    Map<String, dynamic> _bodies = _getBodies(body);

    return clockRequest(
        path: url,
        options: BaseOptions(
            baseUrl: baseAPI, followRedirects: false, headers: _headers),
        action: (path, dio) async {
          return await dio.post(path, data: _bodies);
        });
  }

  Future<Response> put(String url,
      {Map body,
      Map<String, String> headers,
      Map<String, dynamic> params}) async {
    Map<String, String> _headers = _getHeaders(headers);
    Map<String, dynamic> _params = params ?? {};
    Map<String, dynamic> _bodies = _getBodies(body);

    return clockRequest(
        path: url,
        options: BaseOptions(
            baseUrl: baseAPI,
            queryParameters: _params,
            followRedirects: false,
            headers: _headers),
        action: (path, dio) async {
          return await dio.put(path, data: _bodies);
        });
  }

  Future<Response> delete(String url,
      {Map<String, String> headers, Map body}) async {
    Map<String, String> _headers = _getHeaders(headers);
    Map<String, dynamic> _bodies = _getBodies(body);

    return clockRequest(
        path: url,
        options: BaseOptions(
            baseUrl: baseAPI, followRedirects: false, headers: _headers),
        action: (path, dio) async {
          return await dio.delete(path, data: _bodies);
        });
  }

  //==============API================
  Future<Response> authenticate(
      {String fullName, String email, String firebaseToken}) {
    return post("/user/authenticate", body: {
      "fullName": fullName,
      "email": email,
      "firebaseToken": firebaseToken
    });
  }

  Future<Response> login({@required String email, @required String pwd}) {
    return post("/user/login", body: {
      "email": email,
      "password": pwd,
    });
  }

  Future<Response> insertQuestion({@required Map<String, dynamic> question}) {
    return post("/question/insert", body: question);
  }

  Future<Response> updateQuestion(
      {@required Map<String, dynamic> questionWithID}) {
    return post("/question/update", body: questionWithID);
  }

  Future<Response> findQuestion() {
    return get("/question/find");
  }

  Future<Response> insertAnswer({@required Map<String, dynamic> answer}) {
    return post("/answer/insert", body: answer);
  }

  Future<Response> insertManyAnswer({@required Map<String, dynamic> answers}) {
    return post("/answer/insertMany", body: answers);
  }
}
