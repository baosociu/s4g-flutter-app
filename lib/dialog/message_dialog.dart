import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:s4g/view/primary_button.dart';

class MessageDialog extends StatelessWidget {
  final String title;
  final String message;

  const MessageDialog({Key key, this.title, @required this.message})
      : super(key: key);

  static show(BuildContext context,
      {String title, @required String message}) async {
    return await showDialog(
        context: context,
        builder: (context) {
          return MessageDialog(title: title, message: message);
        });
  }

  @override
  Widget build(BuildContext context) {
    return Dialog(
      child: Padding(
        padding: EdgeInsets.all(20),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Padding(
              padding: EdgeInsets.symmetric(vertical: 8),
              child: Text(
                title ?? "",
                style: TextStyle(fontSize: 20, fontWeight: FontWeight.w700),
              ),
            ),
            Padding(
              padding: EdgeInsets.symmetric(vertical: 16),
              child: Text(
                message,
                style: TextStyle(fontSize: 16, color: Colors.grey.shade800),
              ),
            ),
            Padding(
              padding: EdgeInsets.only(top: 24),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  PrimaryButton(
                    text: "OK",
                    margin: EdgeInsets.zero,
                    background: Colors.grey.shade200,
                    onPressed: Navigator.of(context).pop,
                  ),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
