import 'dart:io';

import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:s4g/dialog/message_dialog.dart';
import 'package:s4g/http.dart';
import 'package:s4g/model/questions_model.dart';
import 'package:s4g/view/loading_view.dart';
import 'package:s4g/view/primary_button.dart';

class QuestionsDevicePage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _QuestionsDevicePageState();
}

class _QuestionsDevicePageState extends State<QuestionsDevicePage> {
  TextStyle get _textStyle => TextStyle(fontSize: 18);
  List<String> _myAnswers;
  QuestionsModel _questions;
  bool _isLoading = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Questions Device Page"),
      ),
      body: Stack(
        children: [
          FutureBuilder<dynamic>(
            future: _questions == null
                ? Http().findQuestion()
                : Future.value(_questions),
            builder: (context, data) {
              if (data.hasData) {
                if (_questions == null)
                  _questions = QuestionsModel.fromJson(data.data.data);
                if (_myAnswers == null)
                  _myAnswers =
                      List.generate(_questions.data.length, (index) => "");
                return ListView.separated(
                  itemCount: _questions.data.length + 1,
                  padding: EdgeInsets.only(top: 12, left: 12, right: 12),
                  itemBuilder: (context, index) {
                    if (index < _questions.data.length) {
                      Question question = _questions.data[index];
                      return Padding(
                          padding: EdgeInsets.symmetric(vertical: 8),
                          child: question.type.trim().toLowerCase() == "text"
                              ? _buildShortText(question, index)
                              : _buildOptions(question, index));
                    } else {
                      return _bottomView();
                    }
                  },
                  separatorBuilder: (context, index) => SizedBox(height: 4),
                );
              } else
                return Container(
                  alignment: Alignment.center,
                  padding: EdgeInsets.symmetric(vertical: 32, horizontal: 20),
                  child: data.hasError
                      ? Text(data.error.toString())
                      : LoadingView(),
                );
            },
          ),
          if (_isLoading)
            Container(
              child: LoadingView(),
              color: Colors.black12,
            )
        ],
      ),
    );
  }

  Widget _buildShortText(Question question, int indexQuestion) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(question.text),
        Padding(
          padding: EdgeInsets.all(8),
          child: TextFormField(
            style: _textStyle,
            maxLines: 6,
            minLines: 6,
            onChanged: (s) => _myAnswers[indexQuestion] = s,
            decoration: InputDecoration(border: OutlineInputBorder()),
          ),
        )
      ],
    );
  }

  Widget _buildOptions(Question question, int indexQuestion) {
    int _rows = (question.answers.length / 2).ceil();
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(question.text),
        Padding(
            padding: EdgeInsets.all(8),
            child: Column(
              children: List.generate(_rows, (row) {
                return Row(
                  children: List.generate(2, (col) {
                    int index = row * 2 + col;
                    return index < question.answers.length
                        ? _buildOption(question.answers[index], indexQuestion)
                        : SizedBox();
                  }),
                );
              }),
            ))
      ],
    );
  }

  _buildOption(Answers answer, int indexQuestion) {
    var isAnswer = answer.id.toString() == _myAnswers[indexQuestion];
    var background = isAnswer ? Colors.blueAccent : Colors.white;
    var colorText = isAnswer ? Colors.white : Colors.black;
    var fontWeight = isAnswer ? FontWeight.bold : FontWeight.w500;
    return Expanded(
        child: GestureDetector(
      child: Container(
        margin: EdgeInsets.all(2),
        decoration: BoxDecoration(color: background, border: Border.all()),
        padding: EdgeInsets.all(12),
        child: Text(
          answer.text,
          textAlign: TextAlign.center,
          style: TextStyle(color: colorText, fontWeight: fontWeight),
        ),
      ),
      onTap: () =>
          setState(() => _myAnswers[indexQuestion] = answer.id.toString()),
    ));
  }

  Widget _bottomView() {
    return Column(
      children: [
        // SizedBox(height: 8),
        // Text(
        //   "Estimated Value: XX\$",
        //   textAlign: TextAlign.center,
        //   style: TextStyle(fontWeight: FontWeight.bold),
        // ),
        SafeArea(
            child: PrimaryButton(
          text: "Continue",
          onPressed: _onContinue,
        ))
      ],
    );
  }

  Future<void> _onContinue() async {
    setState(() => _isLoading = true);
    var response = await Http().insertManyAnswer(answers: {
      "answers": List.generate(
          _myAnswers.length,
          (index) => {
                "value":
                    _questions.data[index].type.trim().toLowerCase() == "text"
                        ? _myAnswers[index].toString()
                        : int.tryParse(_myAnswers[index]),
                "type": _questions.data[index].type,
                "questionId": _questions.data[index].id,
                "questionAnswerId": int.tryParse(_myAnswers[index]),
                "imei": "11111"
              })
    });
    if (response.statusCode == HttpStatus.ok) {
      Navigator.of(context).pushNamed("final_info");
    } else {
      // Navigator.of(context).pushNamed("final_info");
      MessageDialog.show(context,
          message: response.data["message"] ?? "", title: "Error");
    }
    setState(() => _isLoading = false);
  }
}
