import 'package:flutter/material.dart';
import 'package:s4g/view/primary_button.dart';
import 'package:qrscan/qrscan.dart' as scanner;

class AuthenDevicePage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _AuthenDevicePageState();
}

class _AuthenDevicePageState extends State<AuthenDevicePage> {
  TextStyle get _textStyle => TextStyle(fontSize: 18);
  TextStyle get _textStyleCode =>
      TextStyle(fontSize: 24, fontWeight: FontWeight.w600);
  TextStyle get _textStyleDescription => TextStyle(fontSize: 14);
  TextStyle get _textStyleHighlight =>
      TextStyle(fontSize: 24, fontWeight: FontWeight.bold, color: Colors.white);
  TextEditingController _qrEditingCtrl = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Authenticate Device Page"),
      ),
      body: Padding(
          padding: EdgeInsets.symmetric(horizontal: 12),
          child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Padding(
                    padding: EdgeInsets.all(20),
                    child: Text(
                      "Authenticate device with S4G",
                      style: _textStyle,
                    )),
                _buildScanView(),
                SafeArea(
                    child: PrimaryButton(
                  text: "Authenticate",
                  onPressed: _navigateBuyerAdvicePage,
                ))
              ])),
    );
  }

  _buildScanView() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        PrimaryButton(
          onPressed: _scanQRCode,
          margin: EdgeInsets.zero,
          padding: EdgeInsets.all(12),
          shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(8)),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text("Scan QR code", style: _textStyleHighlight),
              Icon(Icons.camera_alt, color: Colors.white)
            ],
          ),
        ),
        Padding(
          padding: EdgeInsets.all(12),
          child: Text("Enter authenticaation code manually",
              style: _textStyleDescription),
        ),
        TextFormField(
          style: _textStyleCode,
          controller: _qrEditingCtrl,
          textAlign: TextAlign.center,
          maxLength: 6,
          decoration: InputDecoration(
              counter: SizedBox(),
              contentPadding: EdgeInsets.all(8),
              border: OutlineInputBorder(
                  borderSide: BorderSide(color: Colors.blueAccent)),
              hintText: "__  __  __  __  __  __"),
        )
      ],
    );
  }

  _navigateBuyerAdvicePage() {
    Navigator.of(context).pushNamed("buyer_advice");
  }

  _scanQRCode() async {
    String cameraScanResult = await scanner.scan();
    if (cameraScanResult != null) {
      _qrEditingCtrl.text = cameraScanResult;
    }
  }
}
