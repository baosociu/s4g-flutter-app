
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:s4g/dialog/message_dialog.dart';
import 'package:s4g/http.dart';
import 'package:s4g/model/authen_model.dart';
import 'package:s4g/view/loading_view.dart';
import 'package:s4g/view/primary_button.dart';

class ConfirmAccountPage extends StatefulWidget {
  final AuthenModel authenModel;

  const ConfirmAccountPage({Key key, this.authenModel}) : super(key: key);
  @override
  State<StatefulWidget> createState() => _ConfirmAccountPageState();
}

class _ConfirmAccountPageState extends State<ConfirmAccountPage> {
  TextStyle get _textStyle => TextStyle(fontSize: 18);

  TextEditingController _emailEditCtrl = TextEditingController();
  TextEditingController _pwdEditCtrl = TextEditingController();
  bool _isLoading = false;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    if (widget.authenModel != null && widget.authenModel.data != null)
      _emailEditCtrl.text = widget.authenModel.data.email;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text("Confirm Account Page"),
        ),
        body: WillPopScope(
            child: Stack(
              children: [
                Padding(
                  padding: EdgeInsets.symmetric(horizontal: 12),
                  child: Column(
                    children: [
                      Padding(
                        padding: EdgeInsets.symmetric(vertical: 20),
                        child: Text("Connect account", style: _textStyle),
                      ),
                      Expanded(
                          child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                            Padding(
                              padding: EdgeInsets.all(12),
                              child: TextFormField(
                                style: _textStyle,
                                controller: _emailEditCtrl,
                                decoration: InputDecoration(
                                    border: OutlineInputBorder(
                                        borderSide: BorderSide(
                                            color: Colors.blueAccent)),
                                    labelText: "Email Addess"),
                              ),
                            ),
                            Padding(
                              padding: EdgeInsets.all(12),
                              child: TextFormField(
                                style: _textStyle,
                                controller: _pwdEditCtrl,
                                obscureText: true,
                                decoration: InputDecoration(
                                    border: OutlineInputBorder(
                                        borderSide: BorderSide(
                                            color: Colors.blueAccent)),
                                    labelText: "Password"),
                              ),
                            ),
                            PrimaryButton(
                              text: "Connect",
                              onPressed: _onConnect,
                            )
                          ]))
                    ],
                  ),
                ),
                if (_isLoading)
                  Container(
                    child: LoadingView(),
                    color: Colors.black12,
                  )
              ],
            ),
            onWillPop: _showLogoutDialog));
  }

  void _onConnect() async {
    String _email = _emailEditCtrl.text.trim();
    String _pwd = _pwdEditCtrl.text.trim();
    if (_email.isEmpty || _pwd.isEmpty) {
      MessageDialog.show(context, message: "Email and password is required");
    } else {
      setState(() => _isLoading = true);
      var response = await Http().login(email: _email, pwd: _pwd);
      if (response.statusCode == HttpStatus.ok) {
        Http().token = AuthenModel.fromJson(response.data).data.token;
        Navigator.of(context).pushNamed("questions_device");
      } else {
      MessageDialog.show(context,
          message: response.data["message"] ?? "", title: "Error");
      }
      setState(() => _isLoading = false);
    }
  }

  Future<bool> _showLogoutDialog() async {
    await showDialog(
        context: context,
        builder: (context) {
          return Dialog(
            child: Padding(
              padding: EdgeInsets.all(20),
              child: Column(
                mainAxisSize: MainAxisSize.min,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Padding(
                    padding: EdgeInsets.symmetric(vertical: 8),
                    child: Text(
                      "Log out",
                      style:
                          TextStyle(fontSize: 20, fontWeight: FontWeight.w700),
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.symmetric(vertical: 16),
                    child: Text(
                      "Do you want log out ?",
                      style:
                          TextStyle(fontSize: 16, color: Colors.grey.shade800),
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.only(top: 24),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: [
                        PrimaryButton(
                          text: "Yes",
                          margin: EdgeInsets.zero,
                          onPressed: () => Navigator.of(context)
                              .popUntil((route) => route.isFirst),
                        ),
                        SizedBox(width: 12),
                        PrimaryButton(
                          text: "No",
                          margin: EdgeInsets.zero,
                          background: Colors.grey.shade200,
                          onPressed: Navigator.of(context).pop,
                        ),
                      ],
                    ),
                  )
                ],
              ),
            ),
          );
        });
    return false;
  }
}
