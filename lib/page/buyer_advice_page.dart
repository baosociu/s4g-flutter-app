import 'package:flutter/material.dart';
import 'package:s4g/view/primary_button.dart';

class BuyerAdvicePage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _BuyerAdvicePageState();
}

class _BuyerAdvicePageState extends State<BuyerAdvicePage> {
  TextStyle get _textStyle => TextStyle(fontSize: 18);
  TextStyle get _textStyleHighlight =>
      TextStyle(fontSize: 20, fontWeight: FontWeight.bold);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Buyer Advice Page"),
      ),
      body: Padding(
        padding: EdgeInsets.symmetric(horizontal: 12),
        child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Padding(
                padding: EdgeInsets.all(20),
                child: Text("Device authenticated", style: _textStyle),
              ),
              Text(
                  "Instructions on how to remove accounts from the device and factory reset.",
                  textAlign: TextAlign.center,
                  style: _textStyleHighlight),
              SafeArea(
                  child: PrimaryButton(
                text: "Finalize",
                onPressed: _navigateAuthenDevicePage,
              ))
            ]),
      ),
    );
  }

  _navigateAuthenDevicePage() {
    Navigator.of(context).popUntil((route) => route.isFirst);
  }
}
