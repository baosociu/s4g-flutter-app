
import 'dart:io';

import 'package:dio/dio.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:s4g/dialog/message_dialog.dart';
import 'package:s4g/http.dart';
import 'package:s4g/model/authen_model.dart';
import 'package:s4g/view/loading_view.dart';

class InitialPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _InitialPageState();
}

class _InitialPageState extends State<InitialPage> {
  TextStyle get _textStyle => TextStyle(fontSize: 18);
  GoogleSignIn _googleSignIn = GoogleSignIn(
    scopes: [
      'email',
      'https://www.googleapis.com/auth/contacts.readonly',
    ],
  );

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
      _handleSignIn();
    });
  }

  Future<void> _handleSignIn() async {
    await Future.delayed(Duration(milliseconds: 500));
    try {
      await _googleSignIn.signOut();
      GoogleSignInAccount result = await _googleSignIn.signIn();
      if (result == null) {
        await Navigator.of(context)
            .pushNamed("init_failure", arguments: "Sign-in has been canceled");
      } else {
        //login firebase success
        var token = await FirebaseMessaging().getToken();
        Response response = await Http().authenticate(
            fullName: result.displayName,
            email: result.email,
            firebaseToken: token);
        if (response.statusCode == HttpStatus.ok)
          await Navigator.of(context).pushNamed("confirm_account",
              arguments: AuthenModel.fromJson(response.data));
        else
          await MessageDialog.show(context,
              message: response.statusMessage, title: "Error");
        Http().clearSession();
      }

      _handleSignIn();
    } catch (error) {
      print(error);
      await Navigator.of(context).pushNamed("init_failure", arguments: error);
      _handleSignIn();
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Initalization Page"),
      ),
      body: Padding(
        padding: EdgeInsets.symmetric(horizontal: 12),
        child: Column(mainAxisAlignment: MainAxisAlignment.center, children: [
          Padding(
            padding: EdgeInsets.all(12),
            child:
                Text("Initializing device, please wait...", style: _textStyle),
          ),
          // Padding(
          //   padding: EdgeInsets.all(12),
          //   child:
          //       Text("Step X of Y: currenty doing whatever", style: _textStyle),
          // ),
          LoadingView()
        ]),
      ),
    );
  }
}
