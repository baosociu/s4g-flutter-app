import 'package:flutter/material.dart';
import 'package:s4g/view/primary_button.dart';

class InitialFailurePage extends StatefulWidget {
  final error;

  const InitialFailurePage({Key key, this.error}) : super(key: key);

  @override
  State<StatefulWidget> createState() => _InitialFailurePageState();
}

class _InitialFailurePageState extends State<InitialFailurePage> {
  TextStyle get _textStyle => TextStyle(fontSize: 18);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Initalization Failure Page"),
      ),
      body: Container(
        alignment: Alignment.center,
        padding: EdgeInsets.symmetric(horizontal: 12),
        child: Column(mainAxisAlignment: MainAxisAlignment.center, children: [
          Padding(
            padding: EdgeInsets.all(12),
            child: Text("On which steps did we fail", style: _textStyle),
          ),
          if (widget.error != null)
            Padding(
              padding: EdgeInsets.all(12),
              child: Text(
                widget.error.toString(),
                style: _textStyle,
                textAlign: TextAlign.center,
              ),
            ),
          SizedBox(height: 12),
          PrimaryButton(
            text: "Retry",
            onPressed: _navigateInitialPage,
          )
        ]),
      ),
    );
  }

  void _navigateInitialPage() {
    Navigator.of(context).pop();
  }
}
