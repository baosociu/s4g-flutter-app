import 'package:flutter/material.dart';
import 'package:s4g/view/primary_button.dart';

class FinalInfoPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _FinalInfoPageState();
}

class _FinalInfoPageState extends State<FinalInfoPage> {
  TextStyle get _textStyle => TextStyle(fontSize: 18);
  TextStyle get _textStyleHighlight =>
      TextStyle(fontSize: 20, fontWeight: FontWeight.bold);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Final Information Page"),
      ),
      body: Padding(
        padding: EdgeInsets.symmetric(horizontal: 12),
        child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Padding(
                padding: EdgeInsets.all(20),
                child: Text("Device information uploaded", style: _textStyle),
              ),
              Text(
                  "All information about the product has been uploaded to the S4G server. Once the device will be made available in our shop you will receive an email with more details.",
                  textAlign: TextAlign.center,
                  style: _textStyleHighlight),
              SafeArea(
                  child: PrimaryButton(
                text: "Done",
                onPressed: _navigateAuthenDevicePage,
              ))
            ]),
      ),
    );
  }

  _navigateAuthenDevicePage() {
    Navigator.of(context).pushNamed("authen_device");
  }
}
